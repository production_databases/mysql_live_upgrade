#!/usr/bin/env bash
set -euxo pipefail
DATADIR=$1
RECEIVE_PORT=$2
systemctl stop mysql
set +e
find ${DATADIR} -type f | grep -E -v "${DATADIR}\$|${DATADIR}/mysql|performance_schema|information_schema|mysql_upgrade_info|.+\.err|.+\.pid|.+\.flag" | xargs rm -fv
set -e
nc -d -v -l ${RECEIVE_PORT} | xbstream --parallel=2 -v -x -C ${DATADIR}