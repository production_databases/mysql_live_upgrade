SET SQL_LOG_BIN = 0;
GRANT SELECT ON `test`.* TO 'testuser_r'@'%' IDENTIFIED BY 'Testpass.';
GRANT SELECT, INSERT, UPDATE, DELETE ON `test`.* TO 'testuser_w'@'%' IDENTIFIED BY 'Testpass.';
GRANT ALL PRIVILEGES ON `test`.* TO 'testuser_rw'@'%' IDENTIFIED BY 'Testpass.';
GRANT SELECT ON *.* TO 'monitor'@'%' IDENTIFIED BY 'monitor';
FLUSH PRIVILEGES;
SET SQL_LOG_BIN = 1;

