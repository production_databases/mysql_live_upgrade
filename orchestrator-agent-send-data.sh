#!/usr/bin/env bash
set -euxo pipefail
# @TODO think about encryption https://www.percona.com/forums/questions-discussions/percona-xtrabackup/16871-does-innobackupex-support-both-encryption-compression-and-stream-in-parallel
DATADIR=$1
TARGET_HOST=$2
TARGET_PORT=$3
DATABASES=$(sudo mysql -B --skip-column-names -e "SELECT schema_name FROM INFORMATION_SCHEMA.SCHEMATA WHERE schema_name NOT RLIKE '^(mysql|performance_schema|information_schema)$';" | tr "\n" " " | tr -s " ")
mkdir -p $DATADIR
innobackupex --defaults-file=/etc/mysql/my.cnf --databases="${DATABASES}" --slave-info --no-timestamp --parallel=2 --stream=xbstream ${DATADIR} | nc -v ${TARGET_HOST} ${TARGET_PORT}
