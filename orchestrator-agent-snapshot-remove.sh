#!/usr/bin/env bash
set -xeuo pipefail
SNAPSHOTDIR=$1
rm -rfv ${SNAPSHOTDIR}