#!/usr/bin/env bash
set -xeuo pipefail
if [[ "$#" == "3" ]]; then
    mountOptions=$1
    volumeName=$2
    mountPoint=${3:-/tmp/mounted}
else
    volumeName=$1
    mountPoint=${2:-/tmp/mounted}
fi
ln -nvsf ${volumeName} ${mountPoint}