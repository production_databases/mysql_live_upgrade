#!/usr/bin/env bash
set -euxo pipefail
echo TYPE=\"$(df -Th $1 | egrep -v ^Filesystem | tr -s " " | cut -d " " -f 2)\"