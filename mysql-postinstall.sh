#!/usr/bin/env bash
set -xeuo pipefail

cat >/etc/mysql/conf.d/binlog.cnf <<EOF
[mysqld]
server-id = ${MYSQL_SERVER_ID}
report_host = mysql${MYSQL_SERVER_ID}
bind-address    = 0.0.0.0
log_bin         = /var/log/mysql/mysql-bin.log
binlog_format   = STATEMENT
log_slave_updates = ON
gtid_mode       = ON
enforce_gtid_consistency = ON
innodb_file_per_table = ON
EOF

systemctl enable mysql
systemctl restart mysql
systemctl status mysql


mysql -vvv < /vagrant/orchestrator_grants.sql
mysql -vvv < /vagrant/proxysql_grants.sql

mysql -e "SELECT * FROM information_schema.global_variables WHERE variable_name LIKE 'version' OR variable_name LIKE '%log_bin%' OR variable_name LIKE '%binlog%' OR variable_name LIKE '%server_id%' OR variable_name LIKE '%gtid%';"


dpkg -i /vagrant/orchestrator-agent_*.deb
cp -fv /vagrant/orchestrator-agent.conf.json /etc/orchestrator-agent.conf.json
cp -fv /vagrant/orchestrator-agent*.sh /usr/local/orchestrator-agent/

apt-get install -y jq
dpkg -i /vagrant/orchestrator-client_*.deb

echo 'DAEMONOPTS="--verbose --debug"' > /etc/default/orchestrator-agent

systemctl enable orchestrator-agent
systemctl restart orchestrator-agent
systemctl status orchestrator-agent
