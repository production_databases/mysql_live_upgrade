#!/usr/bin/env bash
set -xeuo pipefail

# @TODO remove this repo after resolve https://github.com/github/orchestrator/pull/XXX
curl -sSL https://packagecloud.io/Slach/orchestrator/gpgkey | apt-key add -
curl -sSf "https://packagecloud.io/install/repositories/Slach/orchestrator/config_file.list?os=ubuntu&dist=xenial&source=script" > /etc/apt/sources.list.d/github_orchestrator.list

# @TODO remove this repo after resolve https://github.com/github/orchestrator-agent/pull/18
curl -sSL https://packagecloud.io/Slach/orchestrator-agent/gpgkey | apt-key add -
curl -sSf "https://packagecloud.io/install/repositories/Slach/orchestrator-agent/config_file.list?os=ubuntu&dist=xenial&source=script" > /etc/apt/sources.list.d/github_orchestrator_agent.list

apt-get update

