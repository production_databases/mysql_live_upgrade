# Steps for reproduce live migration mysql5.7 -> mysql8.0

```bash
# Initional installation
MYSQL_VERSION=5.7 vagrant up mysql1 --provision-with=mysql-server-setup
MYSQL_VERSION=5.7 vagrant up mysql2 --provision-with=mysql-server-setup
vagrant up mysql-proxy --provision-with=orchestrator-setup
vagrant up mysql-proxy --provision-with=proxysql-setup

# Live upgrade 
ROW_COUNT=100000 vagrant up mysql-proxy --provision-with=data-load
MYSQL_VERSION=8.0 vagrant up mysql1 --provision-with=mysql-oracle-upgrade 
MYSQL_VERSION=8.0 vagrant up mysql2 --provision-with=mysql-oracle-upgrade 
ROW_COUNT=100000 vagrant up mysql-proxy --provision-with=data-validate 
```
 