#!/usr/bin/env bash
set -xeuo pipefail
BACKUPDIR="/tmp/-mysql-snapshot-$(date +%Y-%d-%m_%H%M%S)/var/lib/mysql"
innobackupex --defaults-file=/etc/mysql/my.cnf --slave-data ${XTRABACKUP_BACKUP_PARAMS:---parallel=2} --no-timestamp ${BACKUPDIR}
innobackupex --defaults-file=/etc/mysql/my.cnf --apply-log ${XTRABACKUP_APPLY_LOG_PARAMS:---use-memory=64MB --parallel=2} --apply-log ${BACKUPDIR}