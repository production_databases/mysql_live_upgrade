#!/usr/bin/env bash
set -xeuo pipefail
echo "deb http://repo.percona.com/apt xenial main" > /etc/apt/sources.list.d/percona.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 9334A25F8507EFA5
apt-get update
