#!/usr/bin/env bash
DATADIR=$(sudo mysql -B --skip-column-names -e "SELECT @@datadir")
systemctl mysql stop
find ${DATADIR} -type f | grep -E -v "${DATADIR}\$|performance_schema|mysql_upgrade_info|.+\.err|.+\.pid|.+\.flag" | xargs rm -fv