SET SQL_LOG_BIN = 0;
GRANT SUPER, PROCESS, GRANT OPTION, REPLICATION SLAVE, REPLICATION CLIENT, RELOAD ON *.* TO 'orchestrator'@'%' IDENTIFIED BY 'orch_topology_password';
GRANT ALL PRIVILEGES ON *.* TO 'orchestrator'@'%' IDENTIFIED BY 'orch_topology_password';
CHANGE MASTER TO master_user='orchestrator', master_password='orch_topology_password';
FLUSH PRIVILEGES;
SET SQL_LOG_BIN = 1;