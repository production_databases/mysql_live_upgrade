#!/usr/bin/env bash
set -xeuo pipefail
key_ids=( 0x8167EE24 0xE3C94F49 0xcbcb082a1bb943db 0xF1656F24C74CD1D8 0x135659e928c12247 )
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys "${key_ids[@]}"
cat >/etc/apt/sources.list.d/mariadb.list <<EOF
deb http://downloads.mariadb.com/MariaDB/mariadb-${MARIADB_VERSION}/repo/ubuntu xenial main
deb http://downloads.mariadb.com/MaxScale/${MAXSCALE_VERSION}/ubuntu xenial main
deb http://downloads.mariadb.com/Tools/ubuntu xenial main
EOF
apt-get update

