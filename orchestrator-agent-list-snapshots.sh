#!/usr/bin/env bash
set -xeuo pipefail
MOUNTPOINT=/tmp
while IFS= read -r s; do
    # Name GroupName Path SnapshotPercent
    printf "\t$s\txtrabackup\t$s\t100.0\n"
done < <(find ${MOUNTPOINT} -name '-mysql-snapshot-*')

