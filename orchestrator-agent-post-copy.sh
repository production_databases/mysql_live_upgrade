#!/usr/bin/env bash
set -xeuo pipefail
systemctl stop mysql
innobackupex --apply-log --use-memory=256MB --parallel=2 /var/lib/mysql/
chown mysql:mysql -R /var/lib/mysql
systemctl start mysql
master_host=$1
master_log_file=$(cat /var/lib/mysql/xtrabackup_binlog_info | cut -d$'\t' -f 1)
master_log_pos=$(cat /var/lib/mysql/xtrabackup_binlog_info | cut -d$'\t' -f 2)

mysql -e "STOP SLAVE"
mysql -e "CHANGE MASTER TO master_host='$master_host', master_user='orchestrator', master_password='orch_topology_password', master_log_file='$master_log_file', master_log_pos=$master_log_pos"
mysql -e "START SLAVE"
mysql -e "SHOW SLAVE STATUS\G"
rm -rfv /var/lib/mysql/xtrabackup_logfile


