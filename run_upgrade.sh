#!/usr/bin/env bash
set -x

set +e
vagrant destroy --force
set -e

# Oracle MySQL Server 5.7 to 8.0
#export MYSQL_VERSION=5.7
#MYSQL_SERVER_ID=1 vagrant up mysql1 --provision-with=mysql-server-setup
#MYSQL_SERVER_ID=2 vagrant up mysql2 --provision-with=mysql-server-setup

# MaridaDb Server
export MARIADB_VERSION=10.1
export MAXSCALE_VERSION=2.2
#MYSQL_SERVER_ID=1 vagrant up mysql1 --provision-with=mariadb-server-setup
#MYSQL_SERVER_ID=2 vagrant up mysql2 --provision-with=mariadb-server-setup


# Percona Server 5.6 to 5.7
export MYSQL_VERSION=5.6

MYSQL_SERVER_ID=1 vagrant up mysql1 --provision-with=percona-server-setup
MYSQL_SERVER_ID=2 vagrant up mysql2 --provision-with=percona-server-setup
vagrant up mysql-proxy --provision-with=orchestrator-setup
vagrant up mysql-proxy --provision-with=proxysql-setup
vagrant ssh mysql-proxy -- "sudo orchestrator -c all-instances"
vagrant up mysql-proxy --provision-with=seed-mysql2-from-mysql1
vagrant ssh mysql2 -- "sudo -H mysql -vvv -e 'SHOW SLAVE STATUS\\G'"

# Live upgrade
export MYSQLBENCH_SECONDS=1800
export MYSQL_VERSION=5.7
#export MYSQL_VERSION=8.0
export MARIADB_VERSION=10.2
vagrant up mysql-proxy --provision-with=mysqlbech
vagrant up mysql-proxy --provision-with=gracefull-stop-slave
vagrant up mysql2 --provision-with=percona-server-upgrade
vagrant up mysql-proxy --provision-with=switchover-slave-as-master
vagrant up mysql1 --provision-with=percona-server-upgrade
vagrant up mysql-proxy --provision-with=return-slave-to-proxysql
vagrant up mysql-proxy --provision-with=data-validate

vagrant ssh mysql1 -- "sudo mysql -e 'SELECT @@version;'"
vagrant ssh mysql2 -- "sudo mysql -e 'SELECT @@version;'"



