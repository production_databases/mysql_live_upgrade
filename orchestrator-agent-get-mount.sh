#!/usr/bin/env bash
set -xeuo pipefail
MOUNT_POINT=$1
if [[ -d "${MOUNT_POINT}" ]]; then
    DEBVICE=$(df -PT -B 1 ${MOUNT_POINT} | sed -e /^Filesystem/d | tr -s " " | cut -d " " -f 1)
    # device path filesystem
    echo "${DEBVICE} ${MOUNT_POINT} ext4"
fi