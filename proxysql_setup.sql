DELETE FROM mysql_servers;
INSERT INTO mysql_servers (hostname, hostgroup_id, port, weight, max_replication_lag) VALUES ('mysql1', 1, 3306, 1000, 10);
INSERT INTO mysql_servers (hostname, hostgroup_id, port, weight, max_replication_lag) VALUES ('mysql2', 2, 3306, 1000, 10);

DELETE FROM mysql_replication_hostgroups;
INSERT INTO mysql_replication_hostgroups (`writer_hostgroup`, `reader_hostgroup`, `comment`) VALUES (1, 2, 'master->slave split hostgroups');

LOAD MYSQL SERVERS TO RUNTIME;
SAVE MYSQL SERVERS TO DISK;

DELETE FROM mysql_users;
INSERT INTO mysql_users (username, password, active, default_hostgroup, default_schema, transaction_persistent) VALUES ('testuser_w', 'Testpass.', 1, 1, 'test', 1);
INSERT INTO mysql_users (username, password, active, default_hostgroup, default_schema, transaction_persistent) VALUES ('testuser_r', 'Testpass.', 1, 2, 'test', 1);
INSERT INTO mysql_users (username, password, active, default_hostgroup, default_schema, transaction_persistent) VALUES ('testuser_rw', 'Testpass.', 1, 1, 'test', 1);
LOAD MYSQL USERS TO RUNTIME;
SAVE MYSQL USERS TO DISK;

DELETE FROM mysql_query_rules;
INSERT INTO mysql_query_rules (username, destination_hostgroup, active) VALUES ('testuser_w', 1, 1);
INSERT INTO mysql_query_rules (username, destination_hostgroup, active) VALUES ('testuser_r', 2, 1);
INSERT INTO mysql_query_rules (username, destination_hostgroup, active, retries, match_digest) VALUES ('testuser_rw', 1, 1, 100, '^SELECT .* FOR UPDATE');
INSERT INTO mysql_query_rules (username, destination_hostgroup, active, retries, match_digest) VALUES ('testuser_rw', 1, 1, 100, '^(INSERT|DELETE|UPDATE|CREATE|ALTER|REPLACE)');
INSERT INTO mysql_query_rules (username, destination_hostgroup, active, retries, match_digest) VALUES ('testuser_rw', 2, 1, 100, '^SELECT');
LOAD MYSQL QUERY RULES TO RUNTIME;
SAVE MYSQL QUERY RULES TO DISK;

# @TODO think about https://github.com/sysown/proxysql/wiki/Monitor-Module read_only monitoring function!

SET mysql-monitor_enabled = 'false';
LOAD MYSQL VARIABLES TO RUNTIME;
SAVE MYSQL VARIABLES TO DISK;

