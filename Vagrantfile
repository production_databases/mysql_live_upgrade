# -*- mode: ruby -*-
# vi: set ft=ruby :
# MySQL live upgrade Vagrant stand

Vagrant.configure("2") do |config|
    config.vm.box = "ubuntu/xenial64"

    config.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--memory", 512]
        vb.customize ["modifyvm", :id, "--cpus", 2]
    end

    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = false

    config.vm.provision "mysql-server-setup", type: "shell", args: ["#{ENV['MYSQL_VERSION']}", "#{ENV['MYSQL_SERVER_ID']}"], inline: <<-SHELL
        set -xeuo pipefail
        export MYSQL_VERSION=$1
        export MYSQL_SERVER_ID=$2
        source /vagrant/bootstrap.sh
        source /vagrant/mysql-repo.sh
        apt-get install -y mysql-community-client=${MYSQL_VERSION}* mysql-client=${MYSQL_VERSION}* mysql-community-server=${MYSQL_VERSION}* mysql-server=${MYSQL_VERSION}*
        source /vagrant/mysql-postinstall.sh
    SHELL

    config.vm.provision "percona-server-setup", type: "shell", args: ["#{ENV['MYSQL_VERSION']}", "#{ENV['MYSQL_SERVER_ID']}"], inline: <<-SHELL
        set -xeuo pipefail
        export MYSQL_VERSION=$1
        export MYSQL_SERVER_ID=$2
        source /vagrant/bootstrap.sh
        source /vagrant/percona-repo.sh
        apt-get install -y percona-server-server-${MYSQL_VERSION} percona-xtrabackup
        # if [[ $MYSQL_VERSION != *"5.6"* ]]; then
        #    apt-get install -y percona-server-tokudb-${MYSQL_VERSION} percona-server-rocksdb-${MYSQL_VERSION}
        # fi
        source /vagrant/mysql-postinstall.sh
    SHELL

    config.vm.provision "mariadb-server-setup", type: "shell", args: ["#{ENV['MARIADB_VERSION']}", "#{ENV['MAXSCALE_VERSION']}", "#{ENV['MYSQL_SERVER_ID']}"], inline: <<-SHELL
        set -xeuo pipefail
        export MARIADB_VERSION=${1:-10.1}
        export MAXSCALE_VERSION=${2:-2.2}
        export MYSQL_SERVER_ID=${3:1}
        source /vagrant/bootstrap.sh
        source /vagrant/mariadb-repo.sh
        apt-get install -y mariadb-server=${MARIADB_VERSION}*
        source /vagrant/mysql-postinstall.sh
    SHELL

    config.vm.provision "orchestrator-setup", type: "shell", inline: <<-SHELL
        set -xeuo pipefail
        source /vagrant/bootstrap.sh
        apt-get install -y jq
        # apt-get install -y orchestrator orchestrator-client orchestrator-cli
        dpkg -i /vagrant/orchestrator_*.deb /vagrant/orchestrator-cli_*.deb /vagrant/orchestrator-client_*.deb
        cp -fv /vagrant/orchestrator.conf.json /etc/orchestrator.conf.json
        echo 'DAEMONOPTS="--verbose --debug http"' > /etc/default/orchestrator
        orchestrator -c redeploy-internal-db
        systemctl enable orchestrator
        systemctl restart orchestrator
        systemctl status orchestrator
    SHELL

    config.vm.provision "proxysql-setup", type: "shell", args: ["#{ENV['MYSQL_VERSION']}"], inline: <<-SHELL
        set -xeuo pipefail
        export MYSQL_VERSION=$1
        export DEBIAN_FRONTEND=noninteractive
        source /vagrant/bootstrap.sh
        source /vagrant/percona-repo.sh
        source /vagrant/mysql-repo.sh
        apt-get install --allow-downgrades -y mysql-community-client=5.7*
        apt-get  install -y proxysql mysql-utilities
        systemctl enable proxysql
        systemctl restart proxysql
        systemctl status proxysql
        echo "Waiting proxysql to launch on 6032..."
        while ! nc -z 127.0.0.1 6032; do
          sleep 0.1
        done
        mysql -vvv -u admin -padmin -h 127.0.0.1 -P6032 < /vagrant/proxysql_setup.sql
    SHELL

    config.vm.provision "seed-mysql2-from-mysql1", type: "shell", inline: <<-SHELL
        set -xeuo pipefail
        mysql -vvv -u admin -padmin -h 127.0.0.1 -P6032 -e "SELECT * FROM mysql_servers"
        orchestrator-client -c api --path agent-mysql-stop/mysql2 | jq -S .
        orchestrator-client -c api --path agent-create-snapshot/mysql1 | jq -S .
        SEED_ID=$(orchestrator-client -c api --path agent-seed/mysql2/mysql1)
        sleep 30
        IS_DONE=$(orchestrator-client -c api --path agent-seed-states/$SEED_ID | jq .[0].Action)
        TRIES=0
        DONE='"Done"'
        while [[ ${IS_DONE} != ${DONE} && $TRIES -ne 20 ]]
        do
          IS_DONE=$(orchestrator-client -c api --path agent-seed-states/$SEED_ID | jq .[0].Action)
          TRIES=$(($TRIES+1))
          if [[ ${IS_DONE} != ${DONE} ]]; then
            sleep 1
          fi
        done

        if [[ ${IS_DONE} != ${DONE} ]]; then
           exit 1
        fi
        mysql -vvv -u admin -padmin -h 127.0.0.1 -P6032 -e "SELECT * FROM mysql_servers"
    SHELL

    config.vm.provision "mysqlbech", type: "shell", args: ["#{ENV['MYSQLBENCH_SECONDS']}"], inline: <<-SHELL
        set -xeuo pipefail
        export MYSQLBENCH_SECONDS=$1
        curl -s https://packagecloud.io/install/repositories/akopytov/sysbench/script.deb.sh | sudo bash
        apt-get install -y sysbench
        mysql -vvv -u admin -padmin -h 127.0.0.1 -P6032 -e "SELECT * FROM mysql_servers"
        sysbench --report-interval=1 --test=/usr/share/sysbench/tests/include/oltp_legacy/oltp.lua --db-driver=mysql --mysql-user=testuser_rw --mysql-password=Testpass. --mysql-host=127.0.0.1 --mysql-port=6033 --mysql-db=test --oltp-read-only=off prepare
        mysql -vvv -h mysql2 -u orchestrator -porch_topology_password -e "SHOW SLAVE STATUS\\G"
        echo $(date +'%Y-%m-%d %H:%M:%S') "Statring sysbench..." &> /var/log/sysbench-processing.log
        screen -d -m bash -c "sysbench --report-interval=1 --threads=1 --time=${MYSQLBENCH_SECONDS} --test=/usr/share/sysbench/tests/include/oltp_legacy/oltp.lua --db-driver=mysql --mysql-user=testuser_rw --mysql-password=Testpass. --mysql-host=127.0.0.1 --mysql-port=6033 --mysql-db=test --oltp-read-only=off run &>>/var/log/sysbench-processing.log"
        mysql -vvv -h mysql2 -u orchestrator -porch_topology_password -e "SHOW SLAVE STATUS\\G"
    SHELL

    config.vm.provision "gracefull-stop-slave", type: "shell", inline: <<-SHELL
        set -xeuo pipefail
        mysql -vvv -u admin -padmin -h 127.0.0.1 -P6032 -e "
            DELETE FROM mysql_servers;
            INSERT INTO mysql_servers (hostname, hostgroup_id, status, port, weight, max_replication_lag) VALUES ('mysql1', 1, 'ONLINE', 3306, 1000, 10);
            INSERT INTO mysql_servers (hostname, hostgroup_id, status, port, weight, max_replication_lag) VALUES ('mysql1', 2, 'ONLINE', 3306, 1000, 10);
            INSERT INTO mysql_servers (hostname, hostgroup_id, status, port, weight, max_replication_lag) VALUES ('mysql2', 2, 'OFFLINE_SOFT', 3306, 1000, 10);
            LOAD MYSQL SERVERS TO RUNTIME;
            SAVE MYSQL SERVERS TO DISK;
        "

        CONNUSED=$(mysql  -B -N -uadmin -padmin -h 127.0.0.1 -P6032 -e "SELECT IFNULL(SUM(ConnUsed),0) FROM stats_mysql_connection_pool WHERE status='OFFLINE_SOFT' AND srv_host='mysql2'")
        TRIES=0
        while [ $CONNUSED -ne 0 -a $TRIES -ne 20 ]
        do
          CONNUSED=$(mysql  -B -N -uadmin -padmin -h 127.0.0.1 -P6032 -e "SELECT IFNULL(SUM(ConnUsed),0) FROM stats_mysql_connection_pool WHERE status='OFFLINE_SOFT' AND srv_host='mysql2'")
          TRIES=$(($TRIES+1))
          if [ $CONNUSED -ne "0" ]; then
            sleep 0.05
          fi
        done
        mysql -vvv -u admin -padmin -h 127.0.0.1 -P6032 -e "SELECT * FROM mysql_servers"
        mysql -vvv -u admin -padmin -h 127.0.0.1 -P6032 -e "SELECT * FROM stats_mysql_connection_pool ORDER BY srv_host, status"
        mysql -vvv -h mysql2 -u orchestrator -porch_topology_password -e "SHOW SLAVE STATUS\\G"
        cat /var/log/sysbench-processing.log
    SHELL

    config.vm.provision "percona-server-upgrade", type: "shell", args: ["#{ENV['MYSQL_VERSION']}"], inline: <<-SHELL
        set -xeuo pipefail
        export MYSQL_VERSION=$1
        export DEBIAN_FRONTEND=noninteractive
        source /vagrant/percona-repo.sh
        sudo -H mysql -vvv -e "SHOW SLAVE STATUS\\G"
        apt-get install -y percona-server-server-${MYSQL_VERSION} percona-xtrabackup
        #if [[ $MYSQL_VERSION != *"5.6"* ]]; then
        #    apt-get install -y percona-server-tokudb-${MYSQL_VERSION} percona-server-rocksdb-${MYSQL_VERSION}
        #fi
        sudo -H mysql_upgrade --force
        systemctl restart mysql
        sudo -H mysql -vvv -e "SHOW SLAVE STATUS\\G"
    SHELL

    config.vm.provision "switchover-slave-as-master", type: "shell", inline: <<-SHELL
        set -xeuo pipefail
        # TODO remove comment when resolve https://github.com/github/orchestrator/issues/428
        # orchestrator-client --debug -c graceful-master-takeover -i mysql2:3306
        echo $(date +'%Y-%m-%d %H:%M:%S')
        mysql -vvv -u admin -padmin -h 127.0.0.1 -P6032 -e "
            DELETE FROM mysql_servers;
            INSERT INTO mysql_servers (hostname, hostgroup_id, status, port, weight, max_replication_lag) VALUES ('mysql1', 1, 'OFFLINE_SOFT', 3306, 1000, 100);
            INSERT INTO mysql_servers (hostname, hostgroup_id, status, port, weight, max_replication_lag) VALUES ('mysql2', 2, 'ONLINE', 3306, 1000, 100);
            LOAD MYSQL SERVERS TO RUNTIME;
            SAVE MYSQL SERVERS TO DISK;
            SELECT * FROM mysql_servers;
        "
        echo $(date +'%Y-%m-%d %H:%M:%S')
        mysqlrpladmin -vvv --timeout=1 --force --discover-slaves-login=orchestrator:orch_topology_password --rpl-user=orchestrator:orch_topology_password --master=orchestrator:orch_topology_password@mysql1 --new-master=orchestrator:orch_topology_password@mysql2 --demote-master switchover
        echo $(date +'%Y-%m-%d %H:%M:%S')
        mysql -vvv -h mysql2 -u orchestrator -porch_topology_password -e "STOP SLAVE\\G"
        mysql -vvv -u admin -padmin -h 127.0.0.1 -P6032 -e "
            DELETE FROM mysql_servers;
            INSERT INTO mysql_servers (hostname, hostgroup_id, status, port, weight, max_replication_lag) VALUES ('mysql2', 1, 'ONLINE', 3306, 1000, 100);
            INSERT INTO mysql_servers (hostname, hostgroup_id, status, port, weight, max_replication_lag) VALUES ('mysql2', 2, 'ONLINE', 3306, 1000, 100);
            INSERT INTO mysql_servers (hostname, hostgroup_id, status, port, weight, max_replication_lag) VALUES ('mysql1', 2, 'OFFLINE_SOFT', 3306, 0, 100);
            LOAD MYSQL SERVERS TO RUNTIME;
            SAVE MYSQL SERVERS TO DISK;
            SELECT * FROM mysql_servers;
        "

        CONNUSED=$(mysql  -B -N -uadmin -padmin -h 127.0.0.1 -P6032 -e "SELECT IFNULL(SUM(ConnUsed),0) FROM stats_mysql_connection_pool WHERE status='OFFLINE_SOFT' AND srv_host='mysql1'")
        TRIES=0
        while [ $CONNUSED -ne 0 -a $TRIES -ne 20 ]
        do
          CONNUSED=$(mysql  -B -N -uadmin -padmin -h 127.0.0.1 -P6032 -e "SELECT IFNULL(SUM(ConnUsed),0) FROM stats_mysql_connection_pool WHERE status='OFFLINE_SOFT' AND srv_host='mysql1'")
          TRIES=$(($TRIES+1))
          if [ $CONNUSED -ne "0" ]; then
            sleep 0.05
          fi
        done

        mysql -vvv -h mysql2 -u orchestrator -porch_topology_password -e "SHOW SLAVE STATUS\\G"
        mysql -vvv -h mysql1 -u orchestrator -porch_topology_password -e "SHOW SLAVE STATUS\\G"
        mysql -vvv -u admin -padmin -h 127.0.0.1 -P6032 -e "SELECT * FROM stats_mysql_connection_pool ORDER BY srv_host, status"
        cat /var/log/sysbench-processing.log
    SHELL

    config.vm.provision "return-slave-to-proxysql", type: "shell", inline: <<-SHELL
        mysql -vvv -u admin -padmin -h 127.0.0.1 -P6032 -e "
            DELETE FROM mysql_servers;
            INSERT INTO mysql_servers (hostname, hostgroup_id, status, port, weight, max_replication_lag) VALUES ('mysql2', 1, 'ONLINE', 3306, 1000, 100);
            INSERT INTO mysql_servers (hostname, hostgroup_id, status, port, weight, max_replication_lag) VALUES ('mysql2', 2, 'OFFLINE_SOFT', 3306, 0, 100);
            INSERT INTO mysql_servers (hostname, hostgroup_id, status, port, weight, max_replication_lag) VALUES ('mysql1', 2, 'ONLINE', 3306, 1000, 100);
            LOAD MYSQL SERVERS TO RUNTIME;
            SAVE MYSQL SERVERS TO DISK;
            SELECT * FROM mysql_servers;
        "
    SHELL


    config.vm.provision "data-validate", type: "shell", args: ["#{ENV['MYSQLBENCH_SECONDS']}"], inline: <<-SHELL
        set -xeuo pipefail
        export MYSQLBENCH_SECONDS=$1
        IS_DONE=$(ps auxf | grep screen | wc -l)
        TRIES=0
        DONE=1
        while [[ ${IS_DONE} != ${DONE} && $TRIES -ne ${MYSQLBENCH_SECONDS} ]]
        do
          IS_DONE=$(ps auxf | grep screen | wc -l)
          TRIES=$(($TRIES+1))
          if [[ ${IS_DONE} != ${DONE} ]]; then
            sleep 1
          fi
        done

        if [[ ${IS_DONE} != ${DONE} ]]; then
           exit 1
        fi
        sysbench --test=/usr/share/sysbench/tests/include/oltp_legacy/oltp.lua --db-driver=mysql --mysql-user=testuser_rw --mysql-password=Testpass. --mysql-host=127.0.0.1 --mysql-port=6033 --mysql-db=test --oltp-read-only=off cleanup
        cat /var/log/sysbench-processing.log
    SHELL


    config.vm.define 'mysql1' do |machine|
        machine.vm.network "private_network", ip: "10.77.7.2"
        machine.vm.host_name = "mysql1"
        machine.hostmanager.aliases = ["mysql1.vagrant.k8s-russian.video"]
        machine.vm.provider :virtualbox do |vb|
            vb.customize ["modifyvm", :id, "--memory", 1024]
        end
    end

    config.vm.define 'mysql2' do |machine|
        machine.vm.network "private_network", ip: "10.77.7.3"
        machine.vm.host_name = "mysql2"
        machine.hostmanager.aliases = ["mysql2.vagrant.k8s-russian.video"]
        machine.vm.provider :virtualbox do |vb|
            vb.customize ["modifyvm", :id, "--memory", 1024]
        end
    end

    config.vm.define 'mysql-proxy' do |machine|
        machine.vm.network "private_network", ip: "10.77.7.4"
        machine.vm.host_name = "mysql-proxy"
        machine.hostmanager.aliases = ["mysql-proxy.vagrant.k8s-russian.video"]
        machine.vm.provider :virtualbox do |vb|
            vb.customize ["modifyvm", :id, "--memory", 512]
        end
    end

end