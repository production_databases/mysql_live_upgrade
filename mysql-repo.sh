#!/usr/bin/env bash
set -xeuo pipefail
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8C718D3B5072E1F5
cat >/etc/apt/sources.list.d/mysql-oracle.list <<EOF
deb http://repo.mysql.com/apt/ubuntu/ xenial mysql-5.7
deb http://repo.mysql.com/apt/ubuntu/ xenial mysql-8.0
deb http://repo.mysql.com/apt/ubuntu/ xenial mysql-tools
EOF
echo mysql-community-server mysql-community-server/root-pass password "" | sudo debconf-set-selections
echo mysql-community-server mysql-community-server/re-root-pass password "" | sudo debconf-set-selections
sudo apt-get update

